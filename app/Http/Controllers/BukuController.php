<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    public function index()
    {
        return Buku::all();
    }

    public function add(Request $request)
    {
        $buku =  Buku::create($request->all());
        return $buku;
    }

    public function detail($id)
    {
        return Buku::find($id);
    }

    public function penerbit($id)
    {
        return Buku::with('penerbit')->find($id);
    }

    public function update(Request $request, $id)
    {

        $buku =  Buku::find($id);
        $buku->update([
            'judul'=>$request->judul,
            'sampul'=>$request->sampul,
            'penerbit_id'=>$request->penerbit_id
        ]);
        return $buku;
    }
}
